(in-package :stumpwm)

(grename "1")
(gnewbg "2")
(gnewbg "3")
(gnewbg "4")
(gnewbg "5")
(gnewbg "6")
(gnewbg "7")
(gnewbg "8")
(gnewbg "9")


;; autostart apps
(run-shell-command "nitrogen --restore")

;; Set prefix key
(set-prefix-key (kbd "C-z"))

;; set init load path

(set-module-dir "~/.stumpwm.d/modules")
(init-load-path *module-dir*)

;; set modules
(load-module "hostname")
(ql:quickload "xembed")

;; Set Colors
(set-bg-color "#0A0A0A")
(set-fg-color "#efefef")
(set-border-color "#efefef")
(set-focus-color "#20424C")
(set-unfocus-color "#112328")
(set-float-focus-color "#20424C")
(set-float-unfocus-color "#112328")
(set-msg-border-width 2)

(setf *startup-message* nil
      *group-format* "%s%t"
      *window-format* "%m%n%s%20t"
      *window-border-style* :thin
      *message-window-padding* 3
      *message-window-y-padding* 3
      *message-window-gravity* :center
      *input-window-gravity* :center
      *input-history-ignore-duplicates* t
      *mouse-focus-policy* :click
      *float-window-border* 2
      *float-window-title-height* 2
      *maxsize-border-width* 2
      *transient-border-width* 2
      *normal-border-width* 2
      *mode-line-highlight-template* "^R~A^r"
      *mode-line-position* :top
      *mode-line-pad-x* 3
      *mode-line-pad-y* 3
      *mode-line-background-color* "#0A0A0A"
      *mode-line-foreground-color* "#efefef"
      *mode-line-timeout* 60
      *mode-line-border-width* 0
      *time-modeline-string* "%a, %e %b %H:%M"
      *screen-mode-line-format* (list "[%g] %W^>%d"))

(define-key *top-map* (kbd "s-b") "windowlist")
(define-key *top-map* (kbd "s-c") "kill-window")
(define-key *top-map* (kbd "s-d") "remove-split")
(define-key *top-map* (kbd "s-f") "float-this")
(define-key *top-map* (kbd "s-F") "unfloat-this")
(define-key *top-map* (kbd "s-h") "hsplit")
(define-key *top-map* (kbd "s-Q") "quit")
(define-key *top-map* (kbd "s-R") "restart-hard")
(define-key *top-map* (kbd "s-t") "fullscreen")
(define-key *top-map* (kbd "s-v") "vsplit")
(define-key *top-map* (kbd "s-x") "exec")
(define-key *top-map* (kbd "s-y") "mode-line")
(define-key *top-map* (kbd "s-Up") "move-focus up")
(define-key *top-map* (kbd "s-Down") "move-focus down")
(define-key *top-map* (kbd "s-Left") "move-focus left")
(define-key *top-map* (kbd "s-Right") "move-focus right")

(dotimes (i 9)
  (let ((val (1+ i)))
    (define-key *top-map*
    (kbd (format nil "s-~d" val))
      (format nil "gselect ~d" val))
    (define-key *top-map*
    (kbd (format nil "M-~d" val))
      (format nil "gmove-and-follow ~d" val))))

;; Toggle mode line display
(toggle-mode-line (current-screen) (current-head))

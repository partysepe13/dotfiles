# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

alias neofetch='neowofetch --source $HOME/Downloads/ascii-art3.txt '

export PATH="/home/partysepe/.local/bin/:$PATH"
